require 'test_helper'

class ExerciceBatchesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exercice_batch = exercice_batches(:one)
  end

  test "should get index" do
    get exercice_batches_url
    assert_response :success
  end

  test "should get new" do
    get new_exercice_batch_url
    assert_response :success
  end

  test "should create exercice_batch" do
    assert_difference('ExerciceBatch.count') do
      post exercice_batches_url, params: { exercice_batch: { name: @exercice_batch.name, subpackage: @exercice_batch.subpackage } }
    end

    assert_redirected_to exercice_batch_url(ExerciceBatch.last)
  end

  test "should show exercice_batch" do
    get exercice_batch_url(@exercice_batch)
    assert_response :success
  end

  test "should get edit" do
    get edit_exercice_batch_url(@exercice_batch)
    assert_response :success
  end

  test "should update exercice_batch" do
    patch exercice_batch_url(@exercice_batch), params: { exercice_batch: { name: @exercice_batch.name, subpackage: @exercice_batch.subpackage } }
    assert_redirected_to exercice_batch_url(@exercice_batch)
  end

  test "should destroy exercice_batch" do
    assert_difference('ExerciceBatch.count', -1) do
      delete exercice_batch_url(@exercice_batch)
    end

    assert_redirected_to exercice_batches_url
  end
end
