require 'test_helper'

class ExerciceGroupsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exercice_group = exercice_groups(:one)
  end

  test "should get index" do
    get exercice_groups_url
    assert_response :success
  end

  test "should get new" do
    get new_exercice_group_url
    assert_response :success
  end

  test "should create exercice_group" do
    assert_difference('ExerciceGroup.count') do
      post exercice_groups_url, params: { exercice_group: { class_name: @exercice_group.class_name, exercice_batch_id: @exercice_group.exercice_batch_id } }
    end

    assert_redirected_to exercice_group_url(ExerciceGroup.last)
  end

  test "should show exercice_group" do
    get exercice_group_url(@exercice_group)
    assert_response :success
  end

  test "should get edit" do
    get edit_exercice_group_url(@exercice_group)
    assert_response :success
  end

  test "should update exercice_group" do
    patch exercice_group_url(@exercice_group), params: { exercice_group: { class_name: @exercice_group.class_name, exercice_batch_id: @exercice_group.exercice_batch_id } }
    assert_redirected_to exercice_group_url(@exercice_group)
  end

  test "should destroy exercice_group" do
    assert_difference('ExerciceGroup.count', -1) do
      delete exercice_group_url(@exercice_group)
    end

    assert_redirected_to exercice_groups_url
  end
end
