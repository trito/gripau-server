require 'test_helper'

class TestExecutionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_execution = test_executions(:one)
  end

  test "should get index" do
    get test_executions_url
    assert_response :success
  end

  test "should get new" do
    get new_test_execution_url
    assert_response :success
  end

  test "should create test_execution" do
    assert_difference('TestExecution.count') do
      post test_executions_url, params: { test_execution: { date: @test_execution.date, exercice_batch_id: @test_execution.exercice_batch_id, user_id: @test_execution.user_id } }
    end

    assert_redirected_to test_execution_url(TestExecution.last)
  end

  test "should show test_execution" do
    get test_execution_url(@test_execution)
    assert_response :success
  end

  test "should get edit" do
    get edit_test_execution_url(@test_execution)
    assert_response :success
  end

  test "should update test_execution" do
    patch test_execution_url(@test_execution), params: { test_execution: { date: @test_execution.date, exercice_batch_id: @test_execution.exercice_batch_id, user_id: @test_execution.user_id } }
    assert_redirected_to test_execution_url(@test_execution)
  end

  test "should destroy test_execution" do
    assert_difference('TestExecution.count', -1) do
      delete test_execution_url(@test_execution)
    end

    assert_redirected_to test_executions_url
  end
end
