require 'test_helper'

class TestFailuresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_failure = test_failures(:one)
  end

  test "should get index" do
    get test_failures_url
    assert_response :success
  end

  test "should get new" do
    get new_test_failure_url
    assert_response :success
  end

  test "should create test_failure" do
    assert_difference('TestFailure.count') do
      post test_failures_url, params: { test_failure: { content: @test_failure.content, message: @test_failure.message, type: @test_failure.type } }
    end

    assert_redirected_to test_failure_url(TestFailure.last)
  end

  test "should show test_failure" do
    get test_failure_url(@test_failure)
    assert_response :success
  end

  test "should get edit" do
    get edit_test_failure_url(@test_failure)
    assert_response :success
  end

  test "should update test_failure" do
    patch test_failure_url(@test_failure), params: { test_failure: { content: @test_failure.content, message: @test_failure.message, type: @test_failure.type } }
    assert_redirected_to test_failure_url(@test_failure)
  end

  test "should destroy test_failure" do
    assert_difference('TestFailure.count', -1) do
      delete test_failure_url(@test_failure)
    end

    assert_redirected_to test_failures_url
  end
end
