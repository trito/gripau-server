require 'test_helper'

class ExerciceTestsExecutionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @exercice_tests_execution = exercice_tests_executions(:one)
  end

  test "should get index" do
    get exercice_tests_executions_url
    assert_response :success
  end

  test "should get new" do
    get new_exercice_tests_execution_url
    assert_response :success
  end

  test "should create exercice_tests_execution" do
    assert_difference('ExerciceTestsExecution.count') do
      post exercice_tests_executions_url, params: { exercice_tests_execution: { exercice_id: @exercice_tests_execution.exercice_id, test_execution_id: @exercice_tests_execution.test_execution_id } }
    end

    assert_redirected_to exercice_tests_execution_url(ExerciceTestsExecution.last)
  end

  test "should show exercice_tests_execution" do
    get exercice_tests_execution_url(@exercice_tests_execution)
    assert_response :success
  end

  test "should get edit" do
    get edit_exercice_tests_execution_url(@exercice_tests_execution)
    assert_response :success
  end

  test "should update exercice_tests_execution" do
    patch exercice_tests_execution_url(@exercice_tests_execution), params: { exercice_tests_execution: { exercice_id: @exercice_tests_execution.exercice_id, test_execution_id: @exercice_tests_execution.test_execution_id } }
    assert_redirected_to exercice_tests_execution_url(@exercice_tests_execution)
  end

  test "should destroy exercice_tests_execution" do
    assert_difference('ExerciceTestsExecution.count', -1) do
      delete exercice_tests_execution_url(@exercice_tests_execution)
    end

    assert_redirected_to exercice_tests_executions_url
  end
end
