require 'test_helper'

class TestCaseExecutionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @test_case_execution = test_case_executions(:one)
  end

  test "should get index" do
    get test_case_executions_url
    assert_response :success
  end

  test "should get new" do
    get new_test_case_execution_url
    assert_response :success
  end

  test "should create test_case_execution" do
    assert_difference('TestCaseExecution.count') do
      post test_case_executions_url, params: { test_case_execution: { exercice_tests_execution_id: @test_case_execution.exercice_tests_execution_id, test_case_id: @test_case_execution.test_case_id, test_failure_id: @test_case_execution.test_failure_id, time: @test_case_execution.time } }
    end

    assert_redirected_to test_case_execution_url(TestCaseExecution.last)
  end

  test "should show test_case_execution" do
    get test_case_execution_url(@test_case_execution)
    assert_response :success
  end

  test "should get edit" do
    get edit_test_case_execution_url(@test_case_execution)
    assert_response :success
  end

  test "should update test_case_execution" do
    patch test_case_execution_url(@test_case_execution), params: { test_case_execution: { exercice_tests_execution_id: @test_case_execution.exercice_tests_execution_id, test_case_id: @test_case_execution.test_case_id, test_failure_id: @test_case_execution.test_failure_id, time: @test_case_execution.time } }
    assert_redirected_to test_case_execution_url(@test_case_execution)
  end

  test "should destroy test_case_execution" do
    assert_difference('TestCaseExecution.count', -1) do
      delete test_case_execution_url(@test_case_execution)
    end

    assert_redirected_to test_case_executions_url
  end
end
