require 'test_helper'

class StudentRepositoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student_repository = student_repositories(:one)
  end

  test "should get index" do
    get student_repositories_url
    assert_response :success
  end

  test "should get new" do
    get new_student_repository_url
    assert_response :success
  end

  test "should create student_repository" do
    assert_difference('StudentRepository.count') do
      post student_repositories_url, params: { student_repository: { exercice_batch_id: @student_repository.exercice_batch_id, git_url: @student_repository.git_url, student_id: @student_repository.student_id } }
    end

    assert_redirected_to student_repository_url(StudentRepository.last)
  end

  test "should show student_repository" do
    get student_repository_url(@student_repository)
    assert_response :success
  end

  test "should get edit" do
    get edit_student_repository_url(@student_repository)
    assert_response :success
  end

  test "should update student_repository" do
    patch student_repository_url(@student_repository), params: { student_repository: { exercice_batch_id: @student_repository.exercice_batch_id, git_url: @student_repository.git_url, student_id: @student_repository.student_id } }
    assert_redirected_to student_repository_url(@student_repository)
  end

  test "should destroy student_repository" do
    assert_difference('StudentRepository.count', -1) do
      delete student_repository_url(@student_repository)
    end

    assert_redirected_to student_repositories_url
  end
end
