require 'test_helper'

class CheckStyleFailuresControllerTest < ActionDispatch::IntegrationTest
  setup do
    @check_style_failure = check_style_failures(:one)
  end

  test "should get index" do
    get check_style_failures_url
    assert_response :success
  end

  test "should get new" do
    get new_check_style_failure_url
    assert_response :success
  end

  test "should create check_style_failure" do
    assert_difference('CheckStyleFailure.count') do
      post check_style_failures_url, params: { check_style_failure: { column: @check_style_failure.column, exercice_tests_execution_id: @check_style_failure.exercice_tests_execution_id, line: @check_style_failure.line, message: @check_style_failure.message, severity: @check_style_failure.severity, source: @check_style_failure.source } }
    end

    assert_redirected_to check_style_failure_url(CheckStyleFailure.last)
  end

  test "should show check_style_failure" do
    get check_style_failure_url(@check_style_failure)
    assert_response :success
  end

  test "should get edit" do
    get edit_check_style_failure_url(@check_style_failure)
    assert_response :success
  end

  test "should update check_style_failure" do
    patch check_style_failure_url(@check_style_failure), params: { check_style_failure: { column: @check_style_failure.column, exercice_tests_execution_id: @check_style_failure.exercice_tests_execution_id, line: @check_style_failure.line, message: @check_style_failure.message, severity: @check_style_failure.severity, source: @check_style_failure.source } }
    assert_redirected_to check_style_failure_url(@check_style_failure)
  end

  test "should destroy check_style_failure" do
    assert_difference('CheckStyleFailure.count', -1) do
      delete check_style_failure_url(@check_style_failure)
    end

    assert_redirected_to check_style_failures_url
  end
end
