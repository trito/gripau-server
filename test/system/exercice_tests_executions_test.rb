require "application_system_test_case"

class ExerciceTestsExecutionsTest < ApplicationSystemTestCase
  setup do
    @exercice_tests_execution = exercice_tests_executions(:one)
  end

  test "visiting the index" do
    visit exercice_tests_executions_url
    assert_selector "h1", text: "Exercice Tests Executions"
  end

  test "creating a Exercice tests execution" do
    visit exercice_tests_executions_url
    click_on "New Exercice Tests Execution"

    fill_in "Exercice", with: @exercice_tests_execution.exercice_id
    fill_in "Test execution", with: @exercice_tests_execution.test_execution_id
    click_on "Create Exercice tests execution"

    assert_text "Exercice tests execution was successfully created"
    click_on "Back"
  end

  test "updating a Exercice tests execution" do
    visit exercice_tests_executions_url
    click_on "Edit", match: :first

    fill_in "Exercice", with: @exercice_tests_execution.exercice_id
    fill_in "Test execution", with: @exercice_tests_execution.test_execution_id
    click_on "Update Exercice tests execution"

    assert_text "Exercice tests execution was successfully updated"
    click_on "Back"
  end

  test "destroying a Exercice tests execution" do
    visit exercice_tests_executions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Exercice tests execution was successfully destroyed"
  end
end
