require "application_system_test_case"

class ExerciceBatchesTest < ApplicationSystemTestCase
  setup do
    @exercice_batch = exercice_batches(:one)
  end

  test "visiting the index" do
    visit exercice_batches_url
    assert_selector "h1", text: "Exercice Batches"
  end

  test "creating a Exercice batch" do
    visit exercice_batches_url
    click_on "New Exercice Batch"

    fill_in "Name", with: @exercice_batch.name
    fill_in "Subpackage", with: @exercice_batch.subpackage
    click_on "Create Exercice batch"

    assert_text "Exercice batch was successfully created"
    click_on "Back"
  end

  test "updating a Exercice batch" do
    visit exercice_batches_url
    click_on "Edit", match: :first

    fill_in "Name", with: @exercice_batch.name
    fill_in "Subpackage", with: @exercice_batch.subpackage
    click_on "Update Exercice batch"

    assert_text "Exercice batch was successfully updated"
    click_on "Back"
  end

  test "destroying a Exercice batch" do
    visit exercice_batches_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Exercice batch was successfully destroyed"
  end
end
