require "application_system_test_case"

class TestCaseExecutionsTest < ApplicationSystemTestCase
  setup do
    @test_case_execution = test_case_executions(:one)
  end

  test "visiting the index" do
    visit test_case_executions_url
    assert_selector "h1", text: "Test Case Executions"
  end

  test "creating a Test case execution" do
    visit test_case_executions_url
    click_on "New Test Case Execution"

    fill_in "Exercice tests execution", with: @test_case_execution.exercice_tests_execution_id
    fill_in "Test case", with: @test_case_execution.test_case_id
    fill_in "Test failure", with: @test_case_execution.test_failure_id
    fill_in "Time", with: @test_case_execution.time
    click_on "Create Test case execution"

    assert_text "Test case execution was successfully created"
    click_on "Back"
  end

  test "updating a Test case execution" do
    visit test_case_executions_url
    click_on "Edit", match: :first

    fill_in "Exercice tests execution", with: @test_case_execution.exercice_tests_execution_id
    fill_in "Test case", with: @test_case_execution.test_case_id
    fill_in "Test failure", with: @test_case_execution.test_failure_id
    fill_in "Time", with: @test_case_execution.time
    click_on "Update Test case execution"

    assert_text "Test case execution was successfully updated"
    click_on "Back"
  end

  test "destroying a Test case execution" do
    visit test_case_executions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Test case execution was successfully destroyed"
  end
end
