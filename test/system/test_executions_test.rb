require "application_system_test_case"

class TestExecutionsTest < ApplicationSystemTestCase
  setup do
    @test_execution = test_executions(:one)
  end

  test "visiting the index" do
    visit test_executions_url
    assert_selector "h1", text: "Test Executions"
  end

  test "creating a Test execution" do
    visit test_executions_url
    click_on "New Test Execution"

    fill_in "Date", with: @test_execution.date
    fill_in "Exercice batch", with: @test_execution.exercice_batch_id
    fill_in "User", with: @test_execution.user_id
    click_on "Create Test execution"

    assert_text "Test execution was successfully created"
    click_on "Back"
  end

  test "updating a Test execution" do
    visit test_executions_url
    click_on "Edit", match: :first

    fill_in "Date", with: @test_execution.date
    fill_in "Exercice batch", with: @test_execution.exercice_batch_id
    fill_in "User", with: @test_execution.user_id
    click_on "Update Test execution"

    assert_text "Test execution was successfully updated"
    click_on "Back"
  end

  test "destroying a Test execution" do
    visit test_executions_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Test execution was successfully destroyed"
  end
end
