require "application_system_test_case"

class TestFailuresTest < ApplicationSystemTestCase
  setup do
    @test_failure = test_failures(:one)
  end

  test "visiting the index" do
    visit test_failures_url
    assert_selector "h1", text: "Test Failures"
  end

  test "creating a Test failure" do
    visit test_failures_url
    click_on "New Test Failure"

    fill_in "Content", with: @test_failure.content
    fill_in "Message", with: @test_failure.message
    fill_in "Type", with: @test_failure.type
    click_on "Create Test failure"

    assert_text "Test failure was successfully created"
    click_on "Back"
  end

  test "updating a Test failure" do
    visit test_failures_url
    click_on "Edit", match: :first

    fill_in "Content", with: @test_failure.content
    fill_in "Message", with: @test_failure.message
    fill_in "Type", with: @test_failure.type
    click_on "Update Test failure"

    assert_text "Test failure was successfully updated"
    click_on "Back"
  end

  test "destroying a Test failure" do
    visit test_failures_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Test failure was successfully destroyed"
  end
end
