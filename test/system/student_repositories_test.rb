require "application_system_test_case"

class StudentRepositoriesTest < ApplicationSystemTestCase
  setup do
    @student_repository = student_repositories(:one)
  end

  test "visiting the index" do
    visit student_repositories_url
    assert_selector "h1", text: "Student Repositories"
  end

  test "creating a Student repository" do
    visit student_repositories_url
    click_on "New Student Repository"

    fill_in "Exercice batch", with: @student_repository.exercice_batch_id
    fill_in "Git url", with: @student_repository.git_url
    fill_in "Student", with: @student_repository.student_id
    click_on "Create Student repository"

    assert_text "Student repository was successfully created"
    click_on "Back"
  end

  test "updating a Student repository" do
    visit student_repositories_url
    click_on "Edit", match: :first

    fill_in "Exercice batch", with: @student_repository.exercice_batch_id
    fill_in "Git url", with: @student_repository.git_url
    fill_in "Student", with: @student_repository.student_id
    click_on "Update Student repository"

    assert_text "Student repository was successfully updated"
    click_on "Back"
  end

  test "destroying a Student repository" do
    visit student_repositories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Student repository was successfully destroyed"
  end
end
