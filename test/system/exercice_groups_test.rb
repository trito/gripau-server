require "application_system_test_case"

class ExerciceGroupsTest < ApplicationSystemTestCase
  setup do
    @exercice_group = exercice_groups(:one)
  end

  test "visiting the index" do
    visit exercice_groups_url
    assert_selector "h1", text: "Exercice Groups"
  end

  test "creating a Exercice group" do
    visit exercice_groups_url
    click_on "New Exercice Group"

    fill_in "Class name", with: @exercice_group.class_name
    fill_in "Exercice batch", with: @exercice_group.exercice_batch_id
    click_on "Create Exercice group"

    assert_text "Exercice group was successfully created"
    click_on "Back"
  end

  test "updating a Exercice group" do
    visit exercice_groups_url
    click_on "Edit", match: :first

    fill_in "Class name", with: @exercice_group.class_name
    fill_in "Exercice batch", with: @exercice_group.exercice_batch_id
    click_on "Update Exercice group"

    assert_text "Exercice group was successfully updated"
    click_on "Back"
  end

  test "destroying a Exercice group" do
    visit exercice_groups_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Exercice group was successfully destroyed"
  end
end
