require "application_system_test_case"

class CheckStyleFailuresTest < ApplicationSystemTestCase
  setup do
    @check_style_failure = check_style_failures(:one)
  end

  test "visiting the index" do
    visit check_style_failures_url
    assert_selector "h1", text: "Check Style Failures"
  end

  test "creating a Check style failure" do
    visit check_style_failures_url
    click_on "New Check Style Failure"

    fill_in "Column", with: @check_style_failure.column
    fill_in "Exercice tests execution", with: @check_style_failure.exercice_tests_execution_id
    fill_in "Line", with: @check_style_failure.line
    fill_in "Message", with: @check_style_failure.message
    fill_in "Severity", with: @check_style_failure.severity
    fill_in "Source", with: @check_style_failure.source
    click_on "Create Check style failure"

    assert_text "Check style failure was successfully created"
    click_on "Back"
  end

  test "updating a Check style failure" do
    visit check_style_failures_url
    click_on "Edit", match: :first

    fill_in "Column", with: @check_style_failure.column
    fill_in "Exercice tests execution", with: @check_style_failure.exercice_tests_execution_id
    fill_in "Line", with: @check_style_failure.line
    fill_in "Message", with: @check_style_failure.message
    fill_in "Severity", with: @check_style_failure.severity
    fill_in "Source", with: @check_style_failure.source
    click_on "Update Check style failure"

    assert_text "Check style failure was successfully updated"
    click_on "Back"
  end

  test "destroying a Check style failure" do
    visit check_style_failures_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Check style failure was successfully destroyed"
  end
end
