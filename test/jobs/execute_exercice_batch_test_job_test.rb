require 'test_helper'

class ExecuteExerciceBatchTestJobTest < ActiveJob::TestCase
  test 'test exercices jog' do
    ExecuteUserRepositoryTestJob.perform_now(StudentRepository.first, true)

    test_execution = TestExecution.last
    assert !test_execution.folder_not_found
    
    
    puts ExerciceTestsExecution.where(test_execution: test_execution).includes(:check_style_failure, test_case_executions: [:test_failures]).to_json
  end
end
