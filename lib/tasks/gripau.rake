namespace :gripau do
  desc "generate student images"
  task :generate_student_images => :environment do
    Student.each do |student|
      student_id = student.id
      student_email = student.email
      `wkhtmltoimage http://localhost:3000/students/#{student_id}/student_page #{student_email}.pdf`
    end
  end

  desc "execute tests"
  task :execute_tests => :environment do
    test_job_executor = TestJobExecutor.new
    test_job_executor.test_all
  end

  # rake gripau:execute_tests_for_batch[9]
  desc "execute tests"
  task :execute_tests_for_batch, [:batch_id] => :environment do |task, args|
    batch_id = args[:batch_id].to_i
    exercice_batch = ExerciceBatch.find(batch_id)
    test_job_executor = TestJobExecutor.new
    test_job_executor.test_batch exercice_batch
  end

  desc "import csv"
  task :import_data => :environment do
    Student.import_all
  end
  # rake gripau:download_sources[13]
  desc "prepares code for exam correction"
  task :download_sources, [:batch_id] => :environment do |task, args|
    batch_id = args[:batch_id].to_i
    exercice_batch = ExerciceBatch.find(batch_id)
    folder = "tmp/exam/#{exercice_batch.id}"
    `rm -rf #{folder}`
    `mkdir -p #{folder}/src`

    exercice_batch.student_repositories.each do |student_repository|
      #dest_folder = "#{folder}/#{student_repository.id}"
      #test_job_executor = TestJobExecutor.new
      #test_job_executor.download_sources(student_repository, exercice_batch, dest_folder)
      dest_folder = TestJobExecutor.dest_folder_for_repo student_repository.student, exercice_batch
      `cp #{dest_folder}/src/* #{folder}/src -r`
    end
    puts "done"
  end


end
