# UserRepository Migration
For each new exerciceBatch we need to create a new student repo.
Proposed new diagram:

classDiagram
    Teacher <|-- Student
    Student <|-- StudentRepository
    ExerciceBatch <|-- ExerciceGroup
    ExerciceGroup <|-- Exercice
    Exercice <|-- TestCase
    ExerciceBatch <|.. StudentRepository :  deleted
    ExerciceBatch <|-- TestExecution
    StudentRepository <|-- TestExecution
    TestExecution <|-- ExerciceTestsExecution
    Exercice <|-- ExerciceTestsExecution
    ExerciceTestsExecution <|-- TestCaseExecution
    TestCase <|-- TestCaseExecution
    TestCaseExecution <|-- TestFailre
    ExerciceTestsExecution <|-- CheckStyleFailure
    TestCase <|-- ManualTestCaseValidation
    Student <|-- ManualTestCaseValidation
    ExerciceBatch --> RepositoryCategory
    StudentRepository --> RepositoryCategory