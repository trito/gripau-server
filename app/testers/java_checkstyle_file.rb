class JavaCheckstyleFile
    attr_accessor :name, :errors

    def self.report_file
        "/reports/checkstyle/main.xml"
    end

    def full_class_name
        @name.split("src/main/java/src/")[1].gsub("/",".")[0..-6]
    end




    def initialize(name, errors)
        @name = name
        @errors = errors
    end

    def failed?
        @errors
    end

    def package_name
        full_class_name.split(".")[4..-2].join(".")
    end

    def class_name
        full_class_name.split(".")[-1]
    end

    def to_s
        "#{@name}: #{self.errors}"
    end

    def self.from_xml(build_path)
        path = "#{build_path}#{report_file}"
        return [] if !File.exist? path
        doc = File.open(path) { |f| Nokogiri::XML(f) }
        filechecks = doc.xpath("//file")
        return filechecks.map{|file| parse_checkstyle_file(file)}
    end

    def self.parse_checkstyle_file(checkstye_file)
        name = checkstye_file.xpath("@name")[0].value
        errors = parse_checkstyle_errors(checkstye_file)
        return self.new name, errors
    end

    def self.parse_checkstyle_errors(checkstye_file)
        errors = checkstye_file.xpath("error")
        return errors.map{|error| parse_checkstyle_error error}
    end

    def self.parse_checkstyle_error(error)
        line = error.xpath("@line")[0].content
        column = error.xpath("@column")[0].try(:content)
        severity = error.xpath("@severity")[0].content
        message = error.xpath("@message")[0].content
        source = error.xpath("@source")[0].content
        return JavaCheckstyleError.new line, column, severity, message, source
    end
end