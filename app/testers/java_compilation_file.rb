class JavaCompilationFile
    attr_accessor :compiled
    def initialize(compiled)
        @compiled = compiled
    end

    def self.from_file(build_path)
        require "json"
        path = "#{build_path}/reports/compile.json"
        compile_data = File.open(path) { |f| JSON.load(f) }
        JavaCompilationFile.new(compile_data["compiled"])
    end
end