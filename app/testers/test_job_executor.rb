class TestJobExecutor < ApplicationJob
  def test_all
  	StudentSubmission.all.each do |submission|
      puts submission.repository.git_url
    	begin
        time = Benchmark.measure {
          test_submission submission
        }
        puts time
	    rescue => exception
	    	puts exception
  			puts exception.backtrace
	    end
    end
  end

  def test_batch exercice_batch, teacher = nil, force = false
    if teacher
      submissions = exercice_batch.student_submissions.for_teacher teacher
    else
      submissions = exercice_batch.student_submissions
    end

  	submissions.each do |submission|
  		begin
  			test_submission submission, force
  		rescue => exception
	    	puts exception
  			puts exception.backtrace
	    end
    end
  end

  def pull_datas
    folder = "../gripau-data"
    shell_call "cd #{folder}\; #{git_command} pull"
  end

  def git_command(ssh_key = nil)
    git_key_path = "../gripau-data/gitkeys/gripau_git_key"
    if ssh_key
      file = Tempfile.new('key')
      file.write(ssh_key)
      file.flush
      git_key_path = file.path
    end

    if File.file?(git_key_path)
      git_command = "GIT_SSH_COMMAND='ssh -i #{File.expand_path(git_key_path)} -o IdentitiesOnly=yes' git"
    else
      git_command = "git"
    end
  end

  def shell_call method
    logger.debug "executing command: #{method}"
    `#{method}`
  end

  # Returns nil if could not connect, list of tags otherwise
  def list_remote_tags(student_repostiory)
    gitkey = student_repostiory.student.teacher.gitkey
    git_url = student_repostiory.git_ssh
    command = "#{git_command(gitkey)} ls-remote #{git_url}"
    result = shell_call(command)
    if(result=="")
      return nil
    else
      return result.match("refs/tags/(.*)\\n"){ |m| m.captures}
    end
  end

  def download_sources(student_submission, exercice_batch, dest_folder)
    student_repository = student_submission.repository
    git_command = git_command(student_submission.teacher.gitkey)
    student = student_submission.student
    repository = student_repository.git_ssh
    if !File.directory?(dest_folder)
      clone_options = ""
      if exercice_batch.git_tag?
        clone_options = "--branch #{exercice_batch.git_tag}"
      end
      shell_call "#{git_command} clone #{clone_options} #{repository} #{dest_folder}"
    else
      pull_options = ""
      if exercice_batch.git_tag?
        pull_options = "origin #{exercice_batch.git_tag}"
      end

      shell_call "cd #{dest_folder}\; #{git_command} pull #{pull_options}"
    end
  end

  def self.dest_folder_for_repo student, exercice_batch
    "tmp/clone/#{student.username}/#{exercice_batch.id}"
  end

  def file_extension_for(language)
    case language
      when "kotlin"
        "kt"
      else
        "java"
      end
  end

  def src_folder_for_language(language)
    case language
      when "kotlin"
        "/src/main/kotlin/"
      else # Java
        "/src/"
      end
  end

  def check_git_error(student_submission)
    exercice_batch = student_submission.exercice_batch
    repository = student_submission.repository
    tags = list_remote_tags(repository)
    if tags == nil
      return "not_found"
    elsif !exercice_batch.git_tag?
      return nil
    elsif tags.include? exercice_batch.git_tag?
      return nil
    else
      return "tag_not_found"
    end
  end

  def test_submission(student_submission, force = false)
    student_repository = student_submission.repository
  	git_get_commit_id = "git rev-parse HEAD"
  	docker_image = "trito/gripau-gradle-tester:0.0.0.21"

    student = student_submission.student
    exercice_batch = student_submission.exercice_batch

    git_error = check_git_error(student_submission)
    if(git_error)
      TestExecution.create(student_submission: student_submission, exercice_batch: exercice_batch, date: Time.now, repository_status: git_error)
      return 
    end

    dest_folder = TestJobExecutor.dest_folder_for_repo student, exercice_batch
    download_sources(student_submission, exercice_batch, dest_folder)

  	commit_id = shell_call "cd #{dest_folder}\; #{git_get_commit_id}"

  	existing_commit = TestExecution.find_by(student_submission: student_submission, exercice_batch: exercice_batch, commit_id: commit_id)
  	if existing_commit && !force
  		puts "Skiping, commit allready tested #{commit_id}"
  		return
  	end

  	src_folder = "#{Dir.pwd}/#{dest_folder}"
  	test_data_folder = File.expand_path(exercice_batch.data_path)
  	result_folder = "#{Dir.pwd}/tmp/result_folder/#{Time.now.to_i.to_s}"
    

    user_package = student.main_package
    user_language = "kotlin" # Todo, enable to change between java and kotlin
    extension = file_extension_for(user_language)

    packages_folders = user_package.gsub(".","/")
  	inside_folder = src_folder_for_language(user_language)+ packages_folders
  	code_folder = dest_folder + inside_folder
  	
    if !File.exists? code_folder
      java_files = Dir.chdir(dest_folder){ ["/**/*.#{extension}"] }
      first_class_path = java_files.empty? ? "" : Dir[dest_folder+"/**/*.#{extension}"][0]
      comment = "#{inside_folder} not found, first class at #{first_class_path}"
      test_execution = TestExecution.create(student_submission: student_submission, exercice_batch: exercice_batch, date: Time.now, compile_success: true, folder_not_found: true, comment: comment, repository_status: "working")
      return
    end

    # only interested in files inside src/cat/itb/.... to avoid other subject files that produce errors
    code_src_folder = "#{src_folder}/#{inside_folder}"
    code_src_gradle_folder  = "/home/gradle/src/main/java/#{packages_folders}"


    ## REMOVE THIS!!!
    # `rm #{code_folder}/m03/uf3 -r`
    # `rm #{code_folder}/dam/m03/uf1/seleccio/*.java`
    # `rm #{code_folder}/dam/m03/uf1/staticfunctions/*.java`
  	
    env_variables = "--env USER_MAIN_PACKAGE=#{user_package} --env USER_LANGUAGE=#{user_language}"
  	report_volume = "-v #{result_folder}/reports:/home/gradle/build/reports"
  	test_results_volume = "-v #{result_folder}/test-results:/home/gradle/build/test-results"
  	gradle_cache_volume = "-v gradle-cache:/home/gradle/.gradle"

  	docker_command = "docker run --rm #{env_variables} -v #{code_src_folder}:#{code_src_gradle_folder} -v #{test_data_folder}:/home/gradle/testdata #{test_results_volume} #{report_volume} #{gradle_cache_volume} #{docker_image}" #  gradle checkstyleMain test

  	puts docker_command
  	docker_command_output = shell_call "#{docker_command} 2>&1"

  	compileInfo = JavaCompilationFile.from_file(result_folder)
    gripau_report = GripauReport.read(result_folder)


  	test_execution = TestExecution.create(student_submission: student_submission, exercice_batch: exercice_batch, date: Time.now, compile_success: compileInfo.compiled, commit_id: commit_id, command_output: docker_command_output, repository_status: "working")
    test_execution.test_report_file.attach(io: File.open(JavaTestCase.test_report_path(result_folder)), filename: 'test_report.xml')

  	return unless test_execution.compile_success

  	java_test_cases = JavaTestCase.from_xml(result_folder)
  	java_test_cases.each do |java_test_case|
  		exercice_group = ExerciceGroup.find_or_create_by(exercice_batch: exercice_batch, name: "TODO_FAKENAME")
  		
  		exercicie_class_name = java_test_case.class_name
  		package_name = java_test_case.package_name
  		test_name = java_test_case.test_name
  		exercice = Exercice.find_or_create_by(exercice_group: exercice_group, class_name: exercicie_class_name, java_package: package_name)
  		
  		source_found = java_test_case.source_found?
      sources = gripau_report ? gripau_report.dig(exercice.class_name, "sources") : nil
  		exercice_tests_execution = ExerciceTestsExecution.find_or_create_by(test_execution: test_execution, exercice: exercice, source_found: source_found) do |e|
        e.sources = sources
      end
    	test_case = TestCase.find_or_create_by(exercice: exercice, name: test_name)
    	
    	if source_found
        test_case_execution = TestCaseExecution.create(exercice_tests_execution: exercice_tests_execution, test_case: test_case, time: java_test_case.time)
        if java_test_case.failed?
          java_failure = java_test_case.failure
				  TestFailure.create(test_case_execution: test_case_execution, message: java_failure.message, failure_type: java_failure.type, content: java_failure.content)
			  end
		  end
  	end

    
    if user_language == "kotlin"
      java_checkstyle_files = DetektFile.from_xml(result_folder)
    else
      java_checkstyle_files = JavaCheckstyleFile.from_xml(result_folder)
    end
  	
  	java_checkstyle_files.each do |java_checkstyle_file|
      #begin
  		  package_name = java_checkstyle_file.package_name
  		  class_name = java_checkstyle_file.class_name
        
    		exercice = Exercice.find_by(java_package: package_name, class_name: class_name)
    		exercice_tests_execution = ExerciceTestsExecution.find_by(test_execution: test_execution, exercice: exercice)
        next if !exercice_tests_execution # error on document of another exercice batch

    		java_checkstyle_file.errors.each do |checkstyle_error|
    			CheckStyleFailure.create!(exercice_tests_execution: exercice_tests_execution, line: checkstyle_error.line, column: checkstyle_error.column, severity: checkstyle_error.severity, message: checkstyle_error.message, source: checkstyle_error.source)
    		end
      #rescue
      #  # just skip checkstyle
      #end
  	end
    test_execution.save # dirty way to update streams

  end
end
