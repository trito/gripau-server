class JavaTestCase
    attr_accessor :name, :time, :failure
    def initialize(name, time, failure)
        @name = name
        @time = time
        @failure = failure
    end

    def failed?
        @failure
    end

    def source_found?
        @failure.try(:type) != "dev.trito.gripau.exceptions.ExerciceClassNotFoundException"
    end

    def to_s
        "#{@name}: #{self.failure&.message}"
    end

    def package_name
        if !name.split(".")[4..-3]
            raise name
        end
        name.split(".")[4..-3].join(".")
    end

    def class_name
        name.split(".")[-2]
    end

    def test_name
        name.split(".")[-1]
    end

    def self.test_report_path(build_path)
        "#{build_path}/test-results/test/TEST-dev.trito.gripau.DynamicTestFromFileTest.xml"
    end

    def self.from_xml(build_path)
        path = test_report_path build_path
        doc = File.open(path) { |f| Nokogiri::XML(f) }
        testcases_xml = doc.xpath("//testcase")
        return testcases_xml.map{|testcase| parse_testcase(testcase)}
    end

    def self.parse_testcase(testcase)
        name = testcase.xpath("@name")[0].value
        time = testcase.xpath("@time")[0].value
        failure = parse_failures(testcase)
        return JavaTestCase.new name, time, failure
    end


    def self.parse_failures(testcase)
        failures = testcase.xpath("failure")
        return nil if failures.empty?
        return parse_failure(failures[0])
    end

    def self.parse_failure(failure)
        content = failure.content
        type = failure.xpath("@type")[0].content
        message = failure.xpath("@message")[0].content
        return JavaTestFailure.new message, type, content
    end
end