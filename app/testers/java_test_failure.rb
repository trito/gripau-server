class JavaTestFailure
    attr_accessor :message, :type, :content
    def initialize(message, type, content)
        @message = message
        @type = type
        @content = content
    end


end