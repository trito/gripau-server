class JavaCheckstyleError
    attr_accessor :line, :column, :severity, :message, :source
    def initialize(line, column, severity, message, source)
        @line = line
        @column = column
        @severity = severity
        @message = message
        @source = source
    end
end