class GripauReport
    def self.report_folder
        "/reports/gripau"
    end

    def self.read(build_path)
        path = "#{build_path}/#{report_folder}"
        return {} if !File.exist? path
        files = Dir["#{path}/*.yml"]
        
        return files.map do |file|
            data = YAML.load(File.read(file))
            [data["name"], data]
        end.to_h
    end
end