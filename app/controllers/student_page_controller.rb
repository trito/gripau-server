class StudentPageController < ApplicationController
  before_action :student_required!
  before_action :set_exercice_batch, only: [:results_for_batch]
  before_action :batch_visivility_is_public_details!, only: [:results_for_batch]
  
  def main
    @student = current_student
  end

  def results_for_batch
    @student_submission = StudentSubmission.find_by(student_id: current_student.id, exercice_batch_id: @exercice_batch.id)
    @student_repository = @student_submission.repository
  end

  private
    def set_exercice_batch
      batch_id = params[:exercice_batch_id]
      @exercice_batch = ExerciceBatch.find(batch_id)
    end
    def batch_visivility_is_public_details!
      if !@exercice_batch.results_visivility_public_details?
        flash[:error] = "No tens permisos per aquesta acció."
        redirect_to student_page_url
      end
    end

end
