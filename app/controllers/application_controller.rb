class ApplicationController < ActionController::Base
	before_action :authenticate_user!

	def current_teacher
		@current_teacer ||= current_user.teacher
	end

	def current_student
		@current_student ||=  current_user.student
	end

	def teacher_required!
		if !current_teacher
			flash[:error] = "No tens permisos per aquesta acció."
			redirect_to destroy_user_session_path
		end
	end

	def student_required!
		if !current_student
			flash[:error] = "No tens permisos per aquesta acció."
			redirect_to destroy_user_session_path
		end
	end

	def my_root
    if current_user
      if current_user.teacher
        redirect_to index_with_results_students_url
      else
        redirect_to student_page_path
      end
    else
      redirect_to new_user_session_path
    end
  end
end
