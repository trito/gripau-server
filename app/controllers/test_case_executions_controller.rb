class TestCaseExecutionsController < ApplicationController
  before_action :teacher_required!
  before_action :set_test_case_execution, only: [:show, :edit, :update, :destroy, :manual_accept]

  # GET /test_case_executions
  # GET /test_case_executions.json
  def index
    @test_case_executions = TestCaseExecution.all
  end

  # GET /test_case_executions/1
  # GET /test_case_executions/1.json
  def show
  end

  # GET /test_case_executions/new
  def new
    @test_case_execution = TestCaseExecution.new
  end

  # GET /test_case_executions/1/edit
  def edit
  end

  def manual_accept
    ManualTestCaseValidation.create!(test_case: @test_case_execution.test_case, student: @test_case_execution.student, passed: true)
    
    render "test_executions/_test_execution_with_data", locals:{test_execution: @test_case_execution.test_execution}
  end

  # POST /test_case_executions
  # POST /test_case_executions.json
  def create
    @test_case_execution = TestCaseExecution.new(test_case_execution_params)

    respond_to do |format|
      if @test_case_execution.save
        format.html { redirect_to @test_case_execution, notice: 'Test case execution was successfully created.' }
        format.json { render :show, status: :created, location: @test_case_execution }
      else
        format.html { render :new }
        format.json { render json: @test_case_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_case_executions/1
  # PATCH/PUT /test_case_executions/1.json
  def update
    respond_to do |format|
      if @test_case_execution.update(test_case_execution_params)
        format.html { redirect_to @test_case_execution, notice: 'Test case execution was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_case_execution }
      else
        format.html { render :edit }
        format.json { render json: @test_case_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_case_executions/1
  # DELETE /test_case_executions/1.json
  def destroy
    @test_case_execution.destroy
    respond_to do |format|
      format.html { redirect_to test_case_executions_url, notice: 'Test case execution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_case_execution
      @test_case_execution = TestCaseExecution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def test_case_execution_params
      params.require(:test_case_execution).permit(:exercice_tests_execution_id, :test_case_id, :time, :test_failure_id)
    end
end
