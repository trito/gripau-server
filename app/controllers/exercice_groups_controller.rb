class ExerciceGroupsController < ApplicationController
  before_action :teacher_required!
  before_action :set_exercice_group, only: [:show, :edit, :update, :destroy]

  # GET /exercice_groups
  # GET /exercice_groups.json
  def index
    @exercice_groups = ExerciceGroup.all
  end

  # GET /exercice_groups/1
  # GET /exercice_groups/1.json
  def show
  end

  # GET /exercice_groups/new
  def new
    @exercice_group = ExerciceGroup.new
  end

  # GET /exercice_groups/1/edit
  def edit
  end

  # POST /exercice_groups
  # POST /exercice_groups.json
  def create
    @exercice_group = ExerciceGroup.new(exercice_group_params)

    respond_to do |format|
      if @exercice_group.save
        format.html { redirect_to @exercice_group, notice: 'Exercice group was successfully created.' }
        format.json { render :show, status: :created, location: @exercice_group }
      else
        format.html { render :new }
        format.json { render json: @exercice_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exercice_groups/1
  # PATCH/PUT /exercice_groups/1.json
  def update
    respond_to do |format|
      if @exercice_group.update(exercice_group_params)
        format.html { redirect_to @exercice_group, notice: 'Exercice group was successfully updated.' }
        format.json { render :show, status: :ok, location: @exercice_group }
      else
        format.html { render :edit }
        format.json { render json: @exercice_group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exercice_groups/1
  # DELETE /exercice_groups/1.json
  def destroy
    @exercice_group.destroy
    respond_to do |format|
      format.html { redirect_to exercice_groups_url, notice: 'Exercice group was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercice_group
      @exercice_group = ExerciceGroup.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def exercice_group_params
      params.require(:exercice_group).permit(:class_name, :exercice_batch_id)
    end
end
