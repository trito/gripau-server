class AdminsController < ApplicationController
  before_action :teacher_required!

  def careful_gripau_console_page
    console
    render inline: '<h2>console page</h2>'
  end

  def careful_upgrade_gripau
    result = `cd /home/gripau/deployer/gripau-server && git pull && bundle install && bundle exec cap production deploy`
    render inline: '<p><%= result %></p>'
  end

  def careful_view_logs
    logs = `tail -n1000 log/production.log`
    render plain: logs
  end

  def login_as
    user = User.find_by(email: params[:email]) || User.from_google(email: params[:email],full_name:nil, uid:nil, avatar_url:nil)
    sign_in(user)
    redirect_to user
  end

  def index

  end

  def update_gripau_data
    TestJobExecutor.new.pull_datas
    ExerciceBatch.import_yml("../gripau-data/exercice_batches.yml")
    flash[:success] = "Imported"
    redirect_to admins_path
  end

  def import_students
    TestJobExecutor.new.pull_datas
    Teacher.import_csv("../gripau-data/teachers.csv")
    Student.import_all
    flash[:success] = "Imported"
    redirect_to admins_path
  end

  def execute_all_tests
    test_job_executor = TestJobExecutor.new
    test_job_executor.test_all
    flash[:success] = "Executing tests in backgound..."
    redirect_to admins_path
  end

  private

end
