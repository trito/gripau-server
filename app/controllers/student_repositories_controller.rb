class StudentRepositoriesController < ApplicationController
  before_action :set_student_repository, only: [:show, :edit, :update, :destroy, :validate]
  before_action :teacher_or_owner, except: [:new, :create]
  before_action :student_required!, only: [:new, :create]

  
  def validate
    ValidateRepositoryJob.perform_later @student_repository
    redirect_to student_repository_path(@student_repository), notice: "Checking, reload in some seconds"
  end

  def import
    @exercice_batches = ExerciceBatch.all
  end

  def do_import
    StudentRepository.import_csv(current_teacher, params[:exercice_batch_id], params[:file])
    redirect_to student_repositories_path, notice: "Repositories imported"
  end

  # GET /student_repositories
  # GET /student_repositories.json
  def index
    @student_repositories = teacher_student_repositories
  end

  # GET /student_repositories/1
  # GET /student_repositories/1.json
  def show
  end

  # GET /student_repositories/new
  def new
    @student_repository = StudentRepository.new
  end

  # GET /student_repositories/1/edit
  def edit
  end

  # POST /student_repositories
  # POST /student_repositories.json
  def create
    @student_repository = current_student.student_repositories.new(student_repository_params)

    respond_to do |format|
      if @student_repository.save
        ValidateRepositoryJob.perform_later @student_repository
        format.html { redirect_to @student_repository, notice: 'Student repository was successfully created.' }
        format.json { render :show, status: :created, location: @student_repository }
      else
        format.html { render :new }
        format.json { render json: @student_repository.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_repositories/1
  # PATCH/PUT /student_repositories/1.json
  def update
    respond_to do |format|
      values = student_repository_params.to_h.merge({"valid_url" => nil})
      if @student_repository.update(values)
        ValidateRepositoryJob.perform_later @student_repository
        format.html { redirect_to @student_repository, notice: 'Student repository was successfully updated.' }
        format.json { render :show, status: :ok, location: @student_repository }
      else
        format.html { render :edit }
        format.json { render json: @student_repository.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_repositories/1
  # DELETE /student_repositories/1.json
  def destroy
    @student_repository.destroy
    respond_to do |format|
      format.html { redirect_to student_repositories_url, notice: 'Student repository was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_repository
      @student_repository = StudentRepository.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_repository_params
      params.require(:student_repository).permit(:git_url)
    end

    def teacher_student_repositories
      StudentRepository.for_teacher current_teacher
    end

    def teacher_or_owner
      if !current_teacher and current_student != @student_repository.student
        flash[:error] = "No tens permisos per aquesta acció."
        redirect_to destroy_user_session_path
      end
    end
end
