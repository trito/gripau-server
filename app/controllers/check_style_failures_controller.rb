class CheckStyleFailuresController < ApplicationController
  before_action :teacher_required!
  before_action :set_check_style_failure, only: [:show, :edit, :update, :destroy]

  # GET /check_style_failures
  # GET /check_style_failures.json
  def index
    @check_style_failures = CheckStyleFailure.all
  end

  # GET /check_style_failures/1
  # GET /check_style_failures/1.json
  def show
  end

  # GET /check_style_failures/new
  def new
    @check_style_failure = CheckStyleFailure.new
  end

  # GET /check_style_failures/1/edit
  def edit
  end

  # POST /check_style_failures
  # POST /check_style_failures.json
  def create
    @check_style_failure = CheckStyleFailure.new(check_style_failure_params)

    respond_to do |format|
      if @check_style_failure.save
        format.html { redirect_to @check_style_failure, notice: 'Check style failure was successfully created.' }
        format.json { render :show, status: :created, location: @check_style_failure }
      else
        format.html { render :new }
        format.json { render json: @check_style_failure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /check_style_failures/1
  # PATCH/PUT /check_style_failures/1.json
  def update
    respond_to do |format|
      if @check_style_failure.update(check_style_failure_params)
        format.html { redirect_to @check_style_failure, notice: 'Check style failure was successfully updated.' }
        format.json { render :show, status: :ok, location: @check_style_failure }
      else
        format.html { render :edit }
        format.json { render json: @check_style_failure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /check_style_failures/1
  # DELETE /check_style_failures/1.json
  def destroy
    @check_style_failure.destroy
    respond_to do |format|
      format.html { redirect_to check_style_failures_url, notice: 'Check style failure was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_check_style_failure
      @check_style_failure = CheckStyleFailure.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def check_style_failure_params
      params.require(:check_style_failure).permit(:exercice_tests_execution_id, :line, :column, :severity, :message, :source)
    end
end
