class TestExecutionsController < ApplicationController
  before_action :teacher_required!
  before_action :set_test_execution, only: [:show, :edit, :update, :destroy]

  # GET /test_executions
  # GET /test_executions.json
  def index
    @test_executions = TestExecution.all
  end

  # GET /test_executions/1
  # GET /test_executions/1.json
  def show
    @exercice_test_executions = ExerciceTestsExecution.where(test_execution: @test_execution).includes(:check_style_failure, test_case_executions: [:test_failures])

  end

  # GET /test_executions/new
  def new
    @test_execution = TestExecution.new
  end

  # GET /test_executions/1/edit
  def edit
  end

  # POST /test_executions
  # POST /test_executions.json
  def create
    @test_execution = TestExecution.new(test_execution_params)

    respond_to do |format|
      if @test_execution.save
        format.html { redirect_to @test_execution, notice: 'Test execution was successfully created.' }
        format.json { render :show, status: :created, location: @test_execution }
      else
        format.html { render :new }
        format.json { render json: @test_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_executions/1
  # PATCH/PUT /test_executions/1.json
  def update
    respond_to do |format|
      if @test_execution.update(test_execution_params)
        format.html { redirect_to @test_execution, notice: 'Test execution was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_execution }
      else
        format.html { render :edit }
        format.json { render json: @test_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_executions/1
  # DELETE /test_executions/1.json
  def destroy
    @test_execution.destroy
    respond_to do |format|
      format.html { redirect_to test_executions_url, notice: 'Test execution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_execution
      @test_execution = TestExecution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def test_execution_params
      params.require(:test_execution).permit(:student_id, :exercice_batch_id, :date)
    end
end
