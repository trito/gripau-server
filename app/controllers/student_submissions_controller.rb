class StudentSubmissionsController < ApplicationController
  before_action :teacher_required!, except: [:execute_tests]
  before_action :set_student_submission, only: [:show, :edit, :update, :destroy, :execute_tests]
  before_action :teacher_or_owner, only: [:execute_tests]

  def execute_tests
    ExecuteUserRepositoryTestJob.perform_later @student_submission, true
  end

  def import
    @exercice_batches = ExerciceBatch.all
  end

  def do_import
    StudentSubmission.import_csv(current_teacher, params[:exercice_batch_id], params[:file])
    redirect_to student_submissions_path, notice: "submissions imported"
  end

  # GET /student_submissions
  # GET /student_submissions.json
  def index
    @student_submissions = teacher_student_submissions
  end

  # GET /student_submissions/1
  # GET /student_submissions/1.json
  def show
  end

  # GET /student_submissions/new
  def new
    @student_submission = StudentSubmission.new
  end

  # GET /student_submissions/1/edit
  def edit
  end

  # POST /student_submissions
  # POST /student_submissions.json
  def create
    @student_submission = StudentSubmission.new(student_submission_params)

    respond_to do |format|
      if @student_submission.save
        format.html { redirect_to @student_submission, notice: 'Student repository was successfully created.' }
        format.json { render :show, status: :created, location: @student_submission }
      else
        format.html { render :new }
        format.json { render json: @student_submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /student_submissions/1
  # PATCH/PUT /student_submissions/1.json
  def update
    respond_to do |format|
      if @student_submission.update(student_submission_params)
        format.html { redirect_to @student_submission, notice: 'Student repository was successfully updated.' }
        format.json { render :show, status: :ok, location: @student_submission }
      else
        format.html { render :edit }
        format.json { render json: @student_submission.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /student_submissions/1
  # DELETE /student_submissions/1.json
  def destroy
    @student_submission.destroy
    respond_to do |format|
      format.html { redirect_to student_submissions_url, notice: 'Student repository was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_student_submission
      @student_submission = StudentSubmission.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def student_submission_params
      params.require(:student_submission).permit(:student_id, :exercice_batch_id, :git_url)
    end

    def teacher_student_submissions
      StudentSubmission.for_teacher current_teacher
    end

    def teacher_or_owner
      if !current_teacher and current_student != @student_submission.student
        flash[:error] = "No tens permisos per aquesta acció."
        redirect_to destroy_user_session_path
      end
    end
end
