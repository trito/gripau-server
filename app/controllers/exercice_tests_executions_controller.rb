class ExerciceTestsExecutionsController < ApplicationController
  before_action :teacher_required!
  before_action :set_exercice_tests_execution, only: [:show, :edit, :update, :destroy]

  # GET /exercice_tests_executions
  # GET /exercice_tests_executions.json
  def index
    @exercice_tests_executions = ExerciceTestsExecution.all
  end

  # GET /exercice_tests_executions/1
  # GET /exercice_tests_executions/1.json
  def show
  end

  # GET /exercice_tests_executions/new
  def new
    @exercice_tests_execution = ExerciceTestsExecution.new
  end

  # GET /exercice_tests_executions/1/edit
  def edit
  end

  # POST /exercice_tests_executions
  # POST /exercice_tests_executions.json
  def create
    @exercice_tests_execution = ExerciceTestsExecution.new(exercice_tests_execution_params)

    respond_to do |format|
      if @exercice_tests_execution.save
        format.html { redirect_to @exercice_tests_execution, notice: 'Exercice tests execution was successfully created.' }
        format.json { render :show, status: :created, location: @exercice_tests_execution }
      else
        format.html { render :new }
        format.json { render json: @exercice_tests_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exercice_tests_executions/1
  # PATCH/PUT /exercice_tests_executions/1.json
  def update
    respond_to do |format|
      if @exercice_tests_execution.update(exercice_tests_execution_params)
        format.html { redirect_to @exercice_tests_execution, notice: 'Exercice tests execution was successfully updated.' }
        format.json { render :show, status: :ok, location: @exercice_tests_execution }
      else
        format.html { render :edit }
        format.json { render json: @exercice_tests_execution.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exercice_tests_executions/1
  # DELETE /exercice_tests_executions/1.json
  def destroy
    @exercice_tests_execution.destroy
    respond_to do |format|
      format.html { redirect_to exercice_tests_executions_url, notice: 'Exercice tests execution was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercice_tests_execution
      @exercice_tests_execution = ExerciceTestsExecution.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def exercice_tests_execution_params
      params.require(:exercice_tests_execution).permit(:test_execution_id, :exercice_id)
    end
end
