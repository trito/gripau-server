class ExerciceBatchesController < ApplicationController
  before_action :teacher_required!
  before_action :set_exercice_batch, only: [:show, :edit, :update, :destroy, :student_results, :execute_tests, :exercices_stats, :export_student_solutions, :student_results_resumee, :make_public]

  # GET /exercice_batches
  # GET /exercice_batches.json
  def index
    @exercice_batches = ExerciceBatch.all
  end

  # GET /exercice_batches/1
  # GET /exercice_batches/1.json
  def show
  end

  def export_student_solutions
    file = @exercice_batch.export_student_solutions(current_teacher)
    result = Rails.root.join("tmp/export/#{file}")
    send_file(result.to_s,:filename => "sources.tar.gz",:type => "mime/type")
  end

  def exercices_stats
    @student_submissions =  @exercice_batch.student_submissions.for_teacher current_teacher
  end

  def student_results
    @student_submissions =  @exercice_batch.student_submissions.for_teacher current_teacher
  end

  def student_results_resumee
    @student_submissions =  @exercice_batch.student_submissions.for_teacher current_teacher
  end

  def execute_tests
    force =  params[:force_test]
    ExecuteExerciceBatchTestJob.perform_later @exercice_batch, current_teacher, force
  end

  def make_public
    @exercice_batch.results_visivility_public_details!
    redirect_to action: "index"  
  end

  # GET /exercice_batches/new
  def new
    @exercice_batch = ExerciceBatch.new
  end

  def import
    ExerciceBatch.import_yml("../gripau-data/exercice_batches.yml")
    redirect_to action: "index"
  end

  # GET /exercice_batches/1/edit
  def edit
  end

  # POST /exercice_batches
  # POST /exercice_batches.json
  def create
    @exercice_batch = ExerciceBatch.new(exercice_batch_params)
    saved = @exercice_batch.save
    StudentRepository.create_all_for(@exercice_batch.id) if saved

    respond_to do |format|
      if @exercice_batch.save
        format.html { redirect_to @exercice_batch, notice: 'Exercice batch was successfully created.' }
        format.json { render :show, status: :created, location: @exercice_batch }
      else
        format.html { render :new }
        format.json { render json: @exercice_batch.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /exercice_batches/1
  # PATCH/PUT /exercice_batches/1.json
  def update
    respond_to do |format|
      if @exercice_batch.update(exercice_batch_params)
        format.html { redirect_to @exercice_batch, notice: 'Exercice batch was successfully updated.' }
        format.json { render :show, status: :ok, location: @exercice_batch }
      else
        format.html { render :edit }
        format.json { render json: @exercice_batch.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /exercice_batches/1
  # DELETE /exercice_batches/1.json
  def destroy
    @exercice_batch.destroy
    respond_to do |format|
      format.html { redirect_to exercice_batches_url, notice: 'Exercice batch was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_exercice_batch
      @exercice_batch = ExerciceBatch.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def exercice_batch_params
      params.require(:exercice_batch).permit(:name, :subpackage, :git_tag, :results_visivility)
    end
end
