class TestFailuresController < ApplicationController
  before_action :teacher_required!
  before_action :set_test_failure, only: [:show, :edit, :update, :destroy]

  # GET /test_failures
  # GET /test_failures.json
  def index
    @test_failures = TestFailure.all
  end

  # GET /test_failures/1
  # GET /test_failures/1.json
  def show
  end

  # GET /test_failures/new
  def new
    @test_failure = TestFailure.new
  end

  # GET /test_failures/1/edit
  def edit
  end

  # POST /test_failures
  # POST /test_failures.json
  def create
    @test_failure = TestFailure.new(test_failure_params)

    respond_to do |format|
      if @test_failure.save
        format.html { redirect_to @test_failure, notice: 'Test failure was successfully created.' }
        format.json { render :show, status: :created, location: @test_failure }
      else
        format.html { render :new }
        format.json { render json: @test_failure.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /test_failures/1
  # PATCH/PUT /test_failures/1.json
  def update
    respond_to do |format|
      if @test_failure.update(test_failure_params)
        format.html { redirect_to @test_failure, notice: 'Test failure was successfully updated.' }
        format.json { render :show, status: :ok, location: @test_failure }
      else
        format.html { render :edit }
        format.json { render json: @test_failure.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /test_failures/1
  # DELETE /test_failures/1.json
  def destroy
    @test_failure.destroy
    respond_to do |format|
      format.html { redirect_to test_failures_url, notice: 'Test failure was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_test_failure
      @test_failure = TestFailure.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def test_failure_params
      params.require(:test_failure).permit(:message, :type, :content)
    end
end
