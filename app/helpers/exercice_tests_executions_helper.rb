module ExerciceTestsExecutionsHelper
  def display_test_results? user, test_execution
    test_execution.results_visible_for? user
  end

  def display_test_details? user
    user.teacher
  end

  #def display_user
  #  user = @simulated_user || current_user
  #end
  #
  #def display_teacher
  #  @display_teacher ||= display_user.teacher
  #end
end
