module TestExecutionsHelper
  def repository_status_as_text test_execution
    if test_execution.repository_working?
      "Working"
    elsif test_execution.repository_tag_not_found?
      "Tag not found"
    elsif test_execution.repository_not_found?
      "Repository not found"
    else
      throw "Invalid option"
    end
    
  end
end
