import { Controller } from "@hotwired/stimulus"
export default class extends Controller {
  static targets = [ "resultDetailContainer" ]

  initialize() {
    
  }



  toggleCheckStyleView() {
    this.resultDetailContainerTargets.forEach((el, i) => {
      el.classList.toggle("hide-check-style");
    })
  }
  toggleSourceCodeView() {
    this.resultDetailContainerTargets.forEach((el, i) => {
      el.classList.toggle("hide-sources");
    })
  }
}