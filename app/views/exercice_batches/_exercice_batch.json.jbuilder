json.extract! exercice_batch, :id, :name, :subpackage, :created_at, :updated_at
json.url exercice_batch_url(exercice_batch, format: :json)
