json.extract! student, :id, :username, :name, :surnames, :email, :created_at, :updated_at
json.url student_url(student, format: :json)
