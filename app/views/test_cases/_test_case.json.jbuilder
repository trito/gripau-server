json.extract! test_case, :id, :exercice_id, :name, :created_at, :updated_at
json.url test_case_url(test_case, format: :json)
