json.extract! test_failure, :id, :message, :type, :content, :created_at, :updated_at
json.url test_failure_url(test_failure, format: :json)
