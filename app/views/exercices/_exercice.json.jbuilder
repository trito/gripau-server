json.extract! exercice, :id, :class_name, :exercice_batch_id, :created_at, :updated_at
json.url exercice_url(exercice, format: :json)
