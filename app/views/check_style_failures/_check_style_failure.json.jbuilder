json.extract! check_style_failure, :id, :exercice_tests_execution_id, :line, :column, :severity, :message, :source, :created_at, :updated_at
json.url check_style_failure_url(check_style_failure, format: :json)
