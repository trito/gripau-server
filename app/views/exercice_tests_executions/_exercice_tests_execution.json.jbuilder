json.extract! exercice_tests_execution, :id, :test_execution_id, :exercice_id, :created_at, :updated_at
json.url exercice_tests_execution_url(exercice_tests_execution, format: :json)
