json.extract! test_case_execution, :id, :exercice_tests_execution_id, :test_case_id, :time, :test_failure_id, :created_at, :updated_at
json.url test_case_execution_url(test_case_execution, format: :json)
