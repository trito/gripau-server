json.extract! test_execution, :id, :student_id, :exercice_batch_id, :date, :created_at, :updated_at
json.url test_execution_url(test_execution, format: :json)
