json.extract! exercice_group, :id, :class_name, :exercice_batch_id, :created_at, :updated_at
json.url exercice_group_url(exercice_group, format: :json)
