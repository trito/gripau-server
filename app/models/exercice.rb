class Exercice < ApplicationRecord
  belongs_to :exercice_group
  has_many :exercice_tests_executions
  has_many :test_cases
end
