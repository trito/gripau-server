class TestExecution < ApplicationRecord
  enum repository_status: { working: 0, tag_not_found: -1, not_found: -2 }, _prefix: :repository

  belongs_to :student_submission
  belongs_to :exercice_batch
  has_many :exercice_tests_executions
  has_one :student, through: :student_submission
  has_one_attached :test_report_file

  scope :last_first, -> { order("created_at DESC") }

  after_save do
    student_submission.update(test_execution: self)
  end

  def resumee
  	total_count = exercice_tests_executions.count
  	not_found_count = exercice_tests_executions.where(source_found: false).count
  	 #failed_count = exercice_tests_executions.joins(test_case_executions: [:test_failures]).distinct("exercice_tests_executions.id").count

    failed_count = exercice_tests_executions.sum do |exercice_tests_execution|
      failed = exercice_tests_execution.test_case_executions.any? do |test_case_execution|
        test_case_execution.failed?
      end
      failed ? 1 : 0
    end
    

  	correct_count = total_count - (not_found_count + failed_count)
  	return {total_count: total_count, not_found_count: not_found_count, failed_count: failed_count, correct_count: correct_count}
  end

  def sources_found?
    exercice_tests_executions.where(source_found: true).exists?
  end

  def tests_available?
    compile_success && sources_found?
  end

  def results_visible_for? user
    !exercice_batch.results_visivility_private? || (user.is_a?(User) && user.teacher?)
  end

end
