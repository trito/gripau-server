class ExerciceGroup < ApplicationRecord
  belongs_to :exercice_batch

  has_many :exercices
end
