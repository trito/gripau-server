class CheckStyleFailure < ApplicationRecord
  belongs_to :exercice_tests_execution

  def naming_issue?
    ["detekt.FunctionNaming", "detekt.VariableNaming"].include? source
  end
end
