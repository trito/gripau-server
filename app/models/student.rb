class Student < ApplicationRecord
  has_one :user
	belongs_to :teacher
	has_many :student_repositories
  has_many :student_submissions
	has_many :test_executions, through: :student_submissions, source: "test_executions"
  has_many :manual_test_case_validations

  def teacher?
  	false
  end

  def student?
  	true
  end

  def default_repository
    student_repositories.last
  end

  def repositories
    student_repositories
  end

  def change_all_repo_urls url
    update!(git_url: url)
    student_repositories.update_all(git_url: url)
    ## TODO: clear caches!
  end

  def course
  	teacher_id == 3 ? "daw" : "dam"
  end

	def main_package
		"cat.itb.#{username.gsub(".", "").gsub("-", "")}.#{course}"
	end

	def self.import_all
		Teacher.all.each do |teacher|
      extension = teacher.email.split("@")[0]
      file = File.new("../gripau-data/students_#{extension}.csv")
      Student.import_csv(teacher, file)  
    end
     
    ExerciceBatch.all.each do |exercice_batch|
      if !StudentSubmission.where(exercice_batch: exercice_batch).exists?
        StudentSubmission.create_all_for(exercice_batch.id)
      end
    end
	end

	def self.import_csv(teacher, file)
		require 'csv'
		CSV.foreach(file.path, headers: false) do |row|
			full_name = row[0]
			email = row[1]
			git_url = row[2]

			next if Student.exists?(teacher: teacher, email: email)
			
			name = full_name.split(",")[1]
			surnames = full_name.split(",")[0]
      username = email.split("@")[0]
      student = Student.create!(teacher: teacher, username: username, name: name, surnames:surnames, email: email)
      StudentRepository.create!(student: student, git_url: git_url)
	  end
	end
end
