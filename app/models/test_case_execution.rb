class TestCaseExecution < ApplicationRecord
  belongs_to :exercice_tests_execution
  belongs_to :test_case
  has_one :student, through: :exercice_tests_execution
  has_one :test_execution, through: :exercice_tests_execution

  has_many :test_failures

  def failed?
	  test_failures.count != 0 && !manualy_accepted?
  end

  def manualy_accepted?
    ManualTestCaseValidation.exists?(student: student, test_case: test_case)
  end
end
