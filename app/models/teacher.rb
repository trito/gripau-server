class Teacher < ApplicationRecord
  has_one :user
  before_save :clean_git_key

  after_create_commit { broadcast_prepend_to :teachers }
  after_update_commit { broadcast_replace_to :teachers}
  after_destroy_commit { broadcast_remove_to :teachers}

	has_many :students

  def self.import_csv file_path="../gripau-data/teachers.csv"
    file = File.new(file_path)
    require 'csv'
    CSV.foreach(file.path, headers: false) do |row|
      email = row[0]
      teacher = Teacher.find_or_create_by(email: email)
    end
  end

  private
    def clean_git_key
      self.gitkey = gitkey.gsub("\r\n", "\n") if gitkey
    end
end
