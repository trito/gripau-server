class StudentSubmission < ApplicationRecord
  include ActionView::RecordIdentifier

  has_many :test_executions
  belongs_to :test_execution, optional: true
  belongs_to :student
  belongs_to :exercice_batch
  belongs_to :student_repository

  has_one :teacher, through: :student
  scope :for_teacher, -> (teacher) { joins(:student).where("students.teacher_id = ?", teacher.id) }

  after_update_commit do
    broadcast_replace_to dom_id(student, "own_results_resumee"), partial: "student_submissions/results_resumee", locals: {student_submission: self, user: student.user}
  end

  def repository
    student_repository
  end

  def self.create_all_for(exercice_batch_id)
    exercice_batch = ExerciceBatch.find(exercice_batch_id)
    Student.all.each do |student|
      student_repository = StudentSubmission.find_or_initialize_by(student: student, exercice_batch: exercice_batch)
      student_repository.student_repository = student.default_repository
      student_repository.save!
    end
  end

end
