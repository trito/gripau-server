class ExerciceTestsExecution < ApplicationRecord
  belongs_to :test_execution
  belongs_to :exercice
  has_one :student, through: :test_execution
  has_many :test_case_executions
  has_many :check_style_failure


  scope :include_failures, -> { includes(:check_style_failure, test_case_executions: [:test_failures]) }

  scope :count_failures, -> {
    left_outer_joins(test_case_executions: [:test_failures]).where("test_failures.failure_type IS NULL OR test_failures.failure_type != 'dev.trito.gripau.exceptions.ExerciceClassNotFoundException'").count
  }

  def success?
  	self.failed_test_cases_count == 0
  end

  def test_cases_count
  	self.test_case_executions.count
  end

  def failed_test_cases_count
  	self.test_case_executions.count { |test_case_execution| test_case_execution.failed?}
  end

  def success_rate
  	return 0 if test_cases_count==0
  	(test_cases_count.to_f - failed_test_cases_count.to_f) / test_cases_count.to_f
  end

end
