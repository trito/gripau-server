class StudentRepository < ApplicationRecord
  has_many :student_submission
  belongs_to :student
  
  def git_ssh
    return git_url if git_url[0..3] != "http"
    
  	start = "https://gitlab.com/".length
  	"git@gitlab.com:#{git_url[start..-1]}"
  end

  #def self.create_all_for(exercice_batch_id)
  #  exercice_batch = ExerciceBatch.find(exercice_batch_id)
  #  Student.all.each do |student|
  #    student_repository = StudentRepository.find_or_initialize_by(student: student, exercice_batch: exercice_batch)
  #    student_repository.git_url = student.git_url
  #    student_repository.save!
  #  end
  #end
end
