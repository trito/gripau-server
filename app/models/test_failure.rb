class TestFailure < ApplicationRecord
	belongs_to :test_case_execution

  def diff
    matches = message.scan(/<.*?>/m)
    return nil if matches.empty?
    Diffy::SplitDiff.new(matches[0][1..-2], matches[1][1..-2], :format => :html)
  end

  def is_input_parsing_failure?
    content.include?("GripauInputParsingException") || content.include?("InputMismatchException") || content.include?("Scanner.throwFor")
  end

  def took_too_long?
    content.include?("execution timed out after")
  end

  def short_description
    if is_input_parsing_failure?
      "Input Failure"
    elsif took_too_long?
      "Timeout"
    elsif content.include?("NotImplementedError")
      "Not Implemented Exception: TODO()"
    elsif content.include?("StackOverflowError")
      "Stack Overflow"
    elsif content.include?("com.fasterxml.jackson.databind.exc")
      "Yaml to Class conversion failure"
    elsif content.include?("NullPointerException")
      "Null Pointer Exception"
    else
      message
    end
  end



  #def diff_text
  #  diff.to_s(:text)
  #end

  #def diff_html
  #  diff.to_s(:html).html_safe
  #end
end
