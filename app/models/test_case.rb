class TestCase < ApplicationRecord
  belongs_to :exercice
  has_many :manual_test_case_validations
  has_many :test_case_execution
end
