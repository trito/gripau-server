class User < ApplicationRecord
  devise :omniauthable, omniauth_providers: [:google_oauth2]
  belongs_to :teacher, optional: true
  belongs_to :student, optional: true

  def teacher?
    teacher_id
  end

  def student?
    student_id
  end

  def email
    teacher.try(:email) || student.try(:email)
  end

  def self.from_google(email:, full_name:, uid:, avatar_url:)
    return nil unless email =~ /@itb.cat\z/
    teacher = Teacher.find_by(email: email)
    student = Student.find_by(email: email)
    return nil unless teacher || student
    create_with(uid: uid, full_name: full_name, avatar_url: avatar_url, teacher: teacher, student: student).find_or_create_by!(email: email)
  end
end
