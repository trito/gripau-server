class ExerciceBatch < ApplicationRecord
	enum results_visivility: { private: 0, public_resumee: 1, public_details: 2 }, _prefix: :results_visivility

	has_many :exercice_groups
	has_many :student_submissions

	def self.addAllStudentsToAllBatches
	  ExerciceBatch.all.each do |exercice_batch|
         StudentSubmission.create_all_for(exercice_batch.id)
      end
	end

	def self.import_yml(file)
		require 'yaml'
		batch_datas = YAML.load_file(file)
		batch_datas.each do |batch_data|
			batch = ExerciceBatch.where(name: batch_data["name"]).first_or_initialize
			batch.data_path = batch_data["data_path"]
			batch.git_tag = batch_data["git_tag"]
			batch.results_visivility = batch_data["secret_results"] ? "private" : "public_resumee"
			batch.save
		end
		ExerciceBatch.addAllStudentsToAllBatches
	end

	def export_student_solutions teacher
		folder = "tmp/export/#{self.id}"
	    `rm -rf #{folder}`
	    `mkdir -p #{folder}/src`
	    
	    self.student_submissions.each do |student_submission|
	      next unless student_submission.teacher == teacher
	      dest_folder = TestJobExecutor.dest_folder_for_repo student_submission.student, self
	      `cp #{dest_folder}/src/* #{folder}/src -r`
	    end
	    result = "#{self.id}.tar.gz"
	    `cd tmp/export/ && tar czf #{result} #{self.id}`
	    result
	end
end
