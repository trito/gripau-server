class ExecuteExerciceBatchTestJob < ApplicationJob
  queue_as :default

  def perform(exercice_batch, teacher = nil, force = false)
  	testJobExecutor = TestJobExecutor.new
  	testJobExecutor.test_batch exercice_batch, teacher, force
  end
end
