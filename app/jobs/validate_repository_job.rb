class ValidateRepositoryJob < ApplicationJob
  queue_as :default

  def perform(student_repository)
    testJobExecutor = TestJobExecutor.new
    tags = testJobExecutor.list_remote_tags student_repository
    connected = tags != nil
    student_repository.update!(valid_url: connected)
  end
end
