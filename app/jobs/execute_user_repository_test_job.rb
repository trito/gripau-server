class ExecuteUserRepositoryTestJob < ApplicationJob
  queue_as :default

  def perform(student_submission, force = false)
  	testJobExecutor = TestJobExecutor.new
  	testJobExecutor.test_submission student_submission, force
  end
end
