class AddSecretResultsToExerciceBatch < ActiveRecord::Migration[6.0]
  def change
    add_column :exercice_batches, :secret_results, :boolean, default: false
  end
end
