class CreateStudents < ActiveRecord::Migration[6.0]
  def change
    create_table :students do |t|
      t.string :username
      t.string :name
      t.string :surnames
      t.string :email

      t.timestamps
    end
  end
end
