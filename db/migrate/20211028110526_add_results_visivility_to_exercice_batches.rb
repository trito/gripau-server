class AddResultsVisivilityToExerciceBatches < ActiveRecord::Migration[6.0]
  def change
    add_column :exercice_batches, :results_visivility, :integer
    ExerciceBatch.all.each do |batch|
      if batch.secret_results
        batch.results_visivility_private!
      else
        batch.results_visivility_public_resumee!
      end
    end
    remove_column :exercice_batches, :secret_results
  end
end
