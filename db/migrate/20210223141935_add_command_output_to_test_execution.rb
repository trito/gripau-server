class AddCommandOutputToTestExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :test_executions, :command_output, :string

  end
end
