class CreateExercices < ActiveRecord::Migration[6.0]
  def change
    create_table :exercices do |t|
      t.string :class_name
      t.string :java_package
      t.references :exercice_group, null: false, foreign_key: true

      t.timestamps
    end
  end
end
