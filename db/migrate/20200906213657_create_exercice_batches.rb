class CreateExerciceBatches < ActiveRecord::Migration[6.0]
  def change
    create_table :exercice_batches do |t|
      t.string :name
      t.string :data_path
      t.timestamps
    end
  end
end
