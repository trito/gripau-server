class AddTeacherIdToStudents < ActiveRecord::Migration[6.0]
  def change
  	change_table :students do |t|
      t.references :teacher
    end
  end
end
