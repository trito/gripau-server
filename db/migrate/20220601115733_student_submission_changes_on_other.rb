class StudentSubmissionChangesOnOther < ActiveRecord::Migration[6.0]
  def change
    add_reference :test_executions, :student_submission, foreign_key: true
    remove_column :test_executions, :student_repository_id
    remove_column :student_repositories, :exercice_batch_id
  end
end
