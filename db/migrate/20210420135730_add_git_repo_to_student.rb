class AddGitRepoToStudent < ActiveRecord::Migration[6.0]
  def change
  	add_column :students, :git_url, :string
  end
end
