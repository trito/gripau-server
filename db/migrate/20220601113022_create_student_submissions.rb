class CreateStudentSubmissions < ActiveRecord::Migration[6.0]
  def change
    create_table :student_submissions do |t|
      t.references :student, null: false, foreign_key: true
      t.references :student_repository, foreign_key: true
      t.references :exercice_batch, null: false, foreign_key: true
      t.references :test_execution, foreign_key: true
      
      t.timestamps
    end
  end
end
