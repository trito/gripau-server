class AddSourceFoundToExerciceTestExecution < ActiveRecord::Migration[6.0]
  def change
  	add_column :exercice_tests_executions, :source_found, :boolean
  end
end
