class AddSourcesToExerciceTestsExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :exercice_tests_executions, :sources, :jsonb
  end
end
