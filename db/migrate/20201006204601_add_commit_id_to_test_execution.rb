class AddCommitIdToTestExecution < ActiveRecord::Migration[6.0]
  def change
	add_column :test_executions, :commit_id, :string
  end
end
