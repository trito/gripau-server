class CreateTestFailures < ActiveRecord::Migration[6.0]
  def change
    create_table :test_failures do |t|
      t.string :message
      t.string :failure_type
      t.text :content
      t.references :test_case_execution, null: false, foreign_key: true

      t.timestamps
    end
  end
end
