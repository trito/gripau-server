class AddManualAcceptanceToTestCaseExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :test_case_executions, :manual_acceptance, :boolean, default: false
  end
end
