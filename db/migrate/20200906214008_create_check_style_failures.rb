class CreateCheckStyleFailures < ActiveRecord::Migration[6.0]
  def change
    create_table :check_style_failures do |t|
      t.references :exercice_tests_execution, null: false, foreign_key: true
      t.integer :line
      t.integer :column
      t.string :severity
      t.string :message
      t.string :source

      t.timestamps
    end
  end
end
