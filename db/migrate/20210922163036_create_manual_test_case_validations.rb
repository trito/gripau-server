class CreateManualTestCaseValidations < ActiveRecord::Migration[6.0]
  def change
    create_table :manual_test_case_validations do |t|
      t.references :student, null: false, foreign_key: true
      t.references :test_case, null: false, foreign_key: true
      t.boolean :passed
      t.timestamps
    end
  end
end
