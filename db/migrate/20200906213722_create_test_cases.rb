class CreateTestCases < ActiveRecord::Migration[6.0]
  def change
    create_table :test_cases do |t|
      t.references :exercice, null: false, foreign_key: true
      t.string :name

      t.timestamps
    end
  end
end
