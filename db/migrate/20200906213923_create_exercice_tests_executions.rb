class CreateExerciceTestsExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :exercice_tests_executions do |t|
      t.references :test_execution, null: false, foreign_key: true
      t.references :exercice, null: false, foreign_key: true

      t.timestamps
    end
  end
end
