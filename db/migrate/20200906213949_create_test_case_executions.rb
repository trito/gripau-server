class CreateTestCaseExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :test_case_executions do |t|
      t.references :exercice_tests_execution, null: false, foreign_key: true
      t.references :test_case, null: false, foreign_key: true
      t.float :time

      t.timestamps
    end
  end
end
