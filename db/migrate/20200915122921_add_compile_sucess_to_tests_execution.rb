class AddCompileSucessToTestsExecution < ActiveRecord::Migration[6.0]
  def change
  	add_column :test_executions, :compile_success, :boolean
  end
end
