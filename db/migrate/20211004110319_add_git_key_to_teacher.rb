class AddGitKeyToTeacher < ActiveRecord::Migration[6.0]
  def change
    add_column :teachers, :gitkey, :string
  end
end
