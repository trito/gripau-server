class CreateTestExecutions < ActiveRecord::Migration[6.0]
  def change
    create_table :test_executions do |t|
      t.references :student_repository, null: false, foreign_key: true
      t.references :exercice_batch, null: false, foreign_key: true
      t.datetime :date

      t.timestamps
    end
  end
end
