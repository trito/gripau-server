class AddCommentToTestExecution < ActiveRecord::Migration[6.0]
  def change
	add_column :test_executions, :comment, :string
	add_column :test_executions, :folder_not_found, :boolean
  end
end
