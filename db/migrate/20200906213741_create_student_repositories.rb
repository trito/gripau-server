class CreateStudentRepositories < ActiveRecord::Migration[6.0]
  def change
    create_table :student_repositories do |t|
      t.references :student, null: false, foreign_key: true
      t.references :exercice_batch, null: false, foreign_key: true
      t.string :git_url

      t.timestamps
    end
  end
end
