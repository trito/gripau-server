class AddUserPackageToStudentRepository < ActiveRecord::Migration[6.0]
  def change
  	add_column :student_repositories, :user_package, :string
  end
end
