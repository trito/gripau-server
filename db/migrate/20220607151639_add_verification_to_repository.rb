class AddVerificationToRepository < ActiveRecord::Migration[6.0]
  def change
    add_column :student_repositories, :valid_url, :boolean
  end
end
