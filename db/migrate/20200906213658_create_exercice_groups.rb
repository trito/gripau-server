class CreateExerciceGroups < ActiveRecord::Migration[6.0]
  def change
    create_table :exercice_groups do |t|
      t.string :name
      t.references :exercice_batch, null: false, foreign_key: true

      t.timestamps
    end
  end
end
