class AddRepoStatusToTestExecution < ActiveRecord::Migration[6.0]
  def change
    add_column :test_executions, :repository_status, :integer, null: false
  end
end
