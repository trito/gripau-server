class AddCurrentTestExecutionToStudentRepository < ActiveRecord::Migration[6.0]
  def change
    add_reference :student_repositories, :test_execution, foreign_key: true
  end
end
