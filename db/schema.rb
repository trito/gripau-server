# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2022_06_07_160252) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "check_style_failures", force: :cascade do |t|
    t.bigint "exercice_tests_execution_id", null: false
    t.integer "line"
    t.integer "column"
    t.string "severity"
    t.string "message"
    t.string "source"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercice_tests_execution_id"], name: "index_check_style_failures_on_exercice_tests_execution_id"
  end

  create_table "exercice_batches", force: :cascade do |t|
    t.string "name"
    t.string "data_path"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "git_tag"
    t.integer "results_visivility"
  end

  create_table "exercice_groups", force: :cascade do |t|
    t.string "name"
    t.bigint "exercice_batch_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercice_batch_id"], name: "index_exercice_groups_on_exercice_batch_id"
  end

  create_table "exercice_tests_executions", force: :cascade do |t|
    t.bigint "test_execution_id", null: false
    t.bigint "exercice_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "source_found"
    t.jsonb "sources"
    t.index ["exercice_id"], name: "index_exercice_tests_executions_on_exercice_id"
    t.index ["test_execution_id"], name: "index_exercice_tests_executions_on_test_execution_id"
  end

  create_table "exercices", force: :cascade do |t|
    t.string "class_name"
    t.string "java_package"
    t.bigint "exercice_group_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercice_group_id"], name: "index_exercices_on_exercice_group_id"
  end

  create_table "manual_test_case_validations", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.bigint "test_case_id", null: false
    t.boolean "passed"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["student_id"], name: "index_manual_test_case_validations_on_student_id"
    t.index ["test_case_id"], name: "index_manual_test_case_validations_on_test_case_id"
  end

  create_table "student_repositories", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.string "git_url"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "user_package"
    t.bigint "test_execution_id"
    t.boolean "valid_url"
    t.index ["student_id"], name: "index_student_repositories_on_student_id"
    t.index ["test_execution_id"], name: "index_student_repositories_on_test_execution_id"
  end

  create_table "student_submissions", force: :cascade do |t|
    t.bigint "student_id", null: false
    t.bigint "student_repository_id"
    t.bigint "exercice_batch_id", null: false
    t.bigint "test_execution_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercice_batch_id"], name: "index_student_submissions_on_exercice_batch_id"
    t.index ["student_id"], name: "index_student_submissions_on_student_id"
    t.index ["student_repository_id"], name: "index_student_submissions_on_student_repository_id"
    t.index ["test_execution_id"], name: "index_student_submissions_on_test_execution_id"
  end

  create_table "students", force: :cascade do |t|
    t.string "username"
    t.string "name"
    t.string "surnames"
    t.string "email"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.bigint "teacher_id"
    t.string "git_url"
    t.index ["teacher_id"], name: "index_students_on_teacher_id"
  end

  create_table "teachers", force: :cascade do |t|
    t.string "email", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "gitkey"
  end

  create_table "test_case_executions", force: :cascade do |t|
    t.bigint "exercice_tests_execution_id", null: false
    t.bigint "test_case_id", null: false
    t.float "time"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "manual_acceptance", default: false
    t.index ["exercice_tests_execution_id"], name: "index_test_case_executions_on_exercice_tests_execution_id"
    t.index ["test_case_id"], name: "index_test_case_executions_on_test_case_id"
  end

  create_table "test_cases", force: :cascade do |t|
    t.bigint "exercice_id", null: false
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["exercice_id"], name: "index_test_cases_on_exercice_id"
  end

  create_table "test_executions", force: :cascade do |t|
    t.bigint "exercice_batch_id", null: false
    t.datetime "date"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.boolean "compile_success"
    t.string "comment"
    t.boolean "folder_not_found"
    t.string "commit_id"
    t.string "command_output"
    t.bigint "student_submission_id"
    t.integer "repository_status", null: false
    t.index ["exercice_batch_id"], name: "index_test_executions_on_exercice_batch_id"
    t.index ["student_submission_id"], name: "index_test_executions_on_student_submission_id"
  end

  create_table "test_failures", force: :cascade do |t|
    t.string "message"
    t.string "failure_type"
    t.text "content"
    t.bigint "test_case_execution_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["test_case_execution_id"], name: "index_test_failures_on_test_case_execution_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "full_name"
    t.string "uid"
    t.string "avatar_url"
    t.bigint "teacher_id"
    t.bigint "student_id"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["student_id"], name: "index_users_on_student_id"
    t.index ["teacher_id"], name: "index_users_on_teacher_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "check_style_failures", "exercice_tests_executions"
  add_foreign_key "exercice_groups", "exercice_batches"
  add_foreign_key "exercice_tests_executions", "exercices"
  add_foreign_key "exercice_tests_executions", "test_executions"
  add_foreign_key "exercices", "exercice_groups"
  add_foreign_key "manual_test_case_validations", "students"
  add_foreign_key "manual_test_case_validations", "test_cases"
  add_foreign_key "student_repositories", "students"
  add_foreign_key "student_repositories", "test_executions"
  add_foreign_key "student_submissions", "exercice_batches"
  add_foreign_key "student_submissions", "student_repositories"
  add_foreign_key "student_submissions", "students"
  add_foreign_key "student_submissions", "test_executions"
  add_foreign_key "test_case_executions", "exercice_tests_executions"
  add_foreign_key "test_case_executions", "test_cases"
  add_foreign_key "test_cases", "exercices"
  add_foreign_key "test_executions", "exercice_batches"
  add_foreign_key "test_executions", "student_submissions"
  add_foreign_key "test_failures", "test_case_executions"
  add_foreign_key "users", "students"
  add_foreign_key "users", "teachers"
end
