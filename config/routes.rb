Rails.application.routes.draw do
  #root :to => "students#index_with_results"
  root :to => "application#my_root"

  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    get 'users/sign_in', to: 'users/sessions#new', as: :new_user_session
    get 'users/sign_out', to: 'users/sessions#destroy', as: :destroy_user_session
  end

  get 'student_page', to: 'student_page#main'
  get 'student_page/results_for_batch', to: 'student_page#results_for_batch'

  resources :admins, only: [:index] do
    collection do
      get :update_gripau_data
      get :execute_all_tests
      get :import_students
      get :careful_gripau_console_page
      get :careful_upgrade_gripau
      get :careful_view_logs
      get :login_as
    end
  end
  

  resources :teachers do
    collection do
      get :update_gripau_data
    end
  end
  resources :exercice_groups
  resources :exercice_batches do
    collection do
      get :import
    end
    member do
      get :make_public
      get :student_results
      get :student_results_resumee
      get :execute_tests
      get :exercices_stats
      get :export_student_solutions
    end
  end

  resources :test_executions
  resources :test_case_executions do
    member do
      get :manual_accept
    end
  end
  resources :check_style_failures
  resources :test_failures
  resources :test_case_executions
  resources :exercice_tests_executions
  
  resources :student_repositories do
    member do
      get :validate
    end
  end

  resources :student_submissions do
    member do
      get :execute_tests
    end
    collection do
      get :import
      post :do_import
    end
  end

  resources :test_cases
  resources :exercices do
    member do
      get :find_exercice_code
    end
  end

  resources :students do 
    collection do
      get :index_with_results
      get :import
      post :do_import
      get :dashboard
    end
    member do 
      get :edit_git_url
      patch :update_git_url
      get :student_page
    end
  end
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
