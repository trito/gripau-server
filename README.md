# README

## Tasks

### Recreate DB
./restartdb.sh
rake gripau:import_data

Execute tests:
rake gripau:execute_tests

## Diagram
[![](https://mermaid.ink/img/eyJjb2RlIjoiY2xhc3NEaWFncmFtXG4gICAgU3R1ZGVudCA8fC0tIFN0dWRlbnRSZXBvc2l0b3J5XG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIEV4ZXJjaWNlR3JvdXBcbiAgICBFeGVyY2ljZUdyb3VwIDx8LS0gRXhlcmNpY2VcbiAgICBFeGVyY2ljZSA8fC0tIFRlc3RDYXNlXG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIFN0dWRlbnRSZXBvc2l0b3J5XG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIFRlc3RFeGVjdXRpb25cbiAgICBTdHVkZW50UmVwb3NpdG9yeSA8fC0tIFRlc3RFeGVjdXRpb25cbiAgICBUZXN0RXhlY3V0aW9uIDx8LS0gRXhlcmNpY2VUZXN0c0V4ZWN1dGlvblxuICAgIEV4ZXJjaWNlIDx8LS0gRXhlcmNpY2VUZXN0c0V4ZWN1dGlvblxuICAgIEV4ZXJjaWNlVGVzdHNFeGVjdXRpb24gPHwtLSBUZXN0Q2FzZUV4ZWN1dGlvblxuICAgIFRlc3RDYXNlIDx8LS0gVGVzdENhc2VFeGVjdXRpb25cbiAgICBUZXN0Q2FzZUV4ZWN1dGlvbiA8fC0tIFRlc3RGYWlscmVcbiAgICBFeGVyY2ljZVRlc3RzRXhlY3V0aW9uIDx8LS0gQ2hlY2tTdHlsZUZhaWx1cmUiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiY2xhc3NEaWFncmFtXG4gICAgU3R1ZGVudCA8fC0tIFN0dWRlbnRSZXBvc2l0b3J5XG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIEV4ZXJjaWNlR3JvdXBcbiAgICBFeGVyY2ljZUdyb3VwIDx8LS0gRXhlcmNpY2VcbiAgICBFeGVyY2ljZSA8fC0tIFRlc3RDYXNlXG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIFN0dWRlbnRSZXBvc2l0b3J5XG4gICAgRXhlcmNpY2VCYXRjaCA8fC0tIFRlc3RFeGVjdXRpb25cbiAgICBTdHVkZW50UmVwb3NpdG9yeSA8fC0tIFRlc3RFeGVjdXRpb25cbiAgICBUZXN0RXhlY3V0aW9uIDx8LS0gRXhlcmNpY2VUZXN0c0V4ZWN1dGlvblxuICAgIEV4ZXJjaWNlIDx8LS0gRXhlcmNpY2VUZXN0c0V4ZWN1dGlvblxuICAgIEV4ZXJjaWNlVGVzdHNFeGVjdXRpb24gPHwtLSBUZXN0Q2FzZUV4ZWN1dGlvblxuICAgIFRlc3RDYXNlIDx8LS0gVGVzdENhc2VFeGVjdXRpb25cbiAgICBUZXN0Q2FzZUV4ZWN1dGlvbiA8fC0tIFRlc3RGYWlscmVcbiAgICBFeGVyY2ljZVRlc3RzRXhlY3V0aW9uIDx8LS0gQ2hlY2tTdHlsZUZhaWx1cmUiLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9LCJ1cGRhdGVFZGl0b3IiOmZhbHNlfQ)

classDiagram
    Teacher <|-- Student
    Student <|-- StudentRepository
    StudentRepository <|-- StudentSubmission
    ExerciceBatch <|-- ExerciceGroup
    ExerciceGroup <|-- Exercice
    Exercice <|-- TestCase
    ExerciceBatch <|-- StudentSubmission
    Student <|-- StudentSubmission
    ExerciceBatch <|-- TestExecution
    StudentSubmission <|-- TestExecution
    TestExecution <|-- ExerciceTestsExecution
    Exercice <|-- ExerciceTestsExecution
    ExerciceTestsExecution <|-- TestCaseExecution
    TestCase <|-- TestCaseExecution
    TestCaseExecution <|-- TestFailre
    ExerciceTestsExecution <|-- CheckStyleFailure
    TestCase <|-- ManualTestCaseValidation
    Student <|-- ManualTestCaseValidation

## Scaffolds

rails g scaffold Student username:string name:string surnames:string email:string
rails g scaffold ExerciceBatch name:string subpackage:string
rails g scaffold Exercice class_name:string exercice_batch:references
rails g scaffold TestCase exercice:references name:string
rails g scaffold StudentRepository student:references exercice_batch:references git_url:string
rails g scaffold TestExecution student:references exercice_batch:references date:datetime
rails g scaffold ExerciceTestsExecution test_execution:references exercice:references
rails g scaffold TestCaseExecution exercice_tests_execution:references test_case:references time:float test_failure:references 
rails g scaffold TestFailure message:string type:string, content:text
rails g scaffold CheckStyleFailure exercice_tests_execution:references line:int column:int severity:stirng message:string source:string

Added Later:
rails g scaffold ExerciceGroup name:string exercice_batch:references



rails g scaffold Teacher username:string email:string

---------------
https://guides.rubyonrails.org/active_job_basics.html

# devise with Google oauth
https://medium.com/@adamlangsner/google-oauth-rails-5-using-devise-and-omniauth-1b7fa5f72c8e


Identificat els exercicis pel seu sub-package

Ideal?


# devise todos
  1. Ensure you have defined default url options in your environments files. Here
     is an example of default_url_options appropriate for a development environment
     in config/environments/development.rb:

       config.action_mailer.default_url_options = { host: 'localhost', port: 3000 }

     In production, :host should be set to the actual host of your application.

     * Required for all applications. *

  2. Ensure you have defined root_url to *something* in your config/routes.rb.
     For example:

       root to: "home#index"
     
     * Not required for API-only Applications *

  3. Ensure you have flash messages in app/views/layouts/application.html.erb.
     For example:

       <p class="notice"><%= notice %></p>
       <p class="alert"><%= alert %></p>

     * Not required for API-only Applications *

  4. You can copy Devise views (for customization) to your app by running:

       rails g devise:views
       
     * Not required *




## Imatges
<span>Photo by <a href="https://unsplash.com/@matthew_t_rader?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Matthew T Rader</a> on <a href="https://unsplash.com/s/photos/toad?utm_source=unsplash&amp;utm_medium=referral&amp;utm_content=creditCopyText">Unsplash</a></span>
https://iconscout.com/icon/frog-face-animal-aquatic
https://iconscout.com/icon/frog-face-animal-aquatic

Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>
https://www.flaticon.com/free-icon/toad_371963


## Future ideas
- itegrate with Theia: https://github.com/eclipse-theia/theia
docker run -it --init -p 3000:3000 -v "$(pwd):/home/project:cached" theiaide/theia:next


